//
//  API.swift
//
//

import Foundation
import SwiftyJSON






typealias JSONDictionary = Dictionary<String, AnyObject>
typealias JSONArray = Array<AnyObject>

class API: NSObject, NSURLConnectionDataDelegate {
    
    enum Path {
        case POSTDATA
    }
    
    class scansettings {
        var mode: String = ""
        var events: String = ""
        var tickettypes: String = ""
        var attractions: String = ""
        var eventpassdates: String = ""
    }
    
    class wcsettings {
        var events: String = ""
        var eventpassdates: String = ""
    }
    
    typealias APICallback = ((JSON, NSError?) -> ())
    let responseData = NSMutableData()
    var statusCode:Int = -1
    var callback: APICallback! = nil
    var path: Path! = nil
    
    
    func signIn(email: String, password: String, callback: APICallback) {
        
        
        var uuid = NSUUID().UUIDString
        let deviceid = "1";
        let brand = "6"
        //let url = "\(Config.baseURL())/api/sessions.json"
        let url = "https://www.flavorus.com/API.aspx"
        
        let body = "email=\(email)&rawpass=\(password.stringByAddingPercentEscapesForQueryValue()!)&deviceid=\(deviceid)&devicetype=ios&salt=\(uuid)&call=SignIn&brand=\(brand)"
        
        makeHTTPPostRequest(Path.POSTDATA, callback: callback, url: url, body: body)
    }
    
    func getEventsFull(authToken:String, callback: APICallback){
        var uuid = NSUUID().UUIDString
        let deviceid = "1";
        let url = "https://www.flavorus.com/API.aspx"
        let body = "authtoken=\(authToken.stringByAddingPercentEscapesForQueryValue()!)&deviceid=\(deviceid)&devicetype=ios&salt=\(uuid)&call=GetEventsFull"
        makeHTTPPostRequest(Path.POSTDATA, callback: callback, url: url, body: body)
    }
    
    func willCallOrders(authToken:String, searchText:String, wcSettings: wcsettings, callback: APICallback){
        var uuid = NSUUID().UUIDString
        let deviceid = "1";
        let url = "https://www.flavorus.com/API.aspx"
        let body = "authtoken=\(authToken.stringByAddingPercentEscapesForQueryValue()!)&deviceid=\(deviceid)&devicetype=ios&salt=\(uuid)&call=WillCallOrders&events=\(wcSettings.events)&eventpassdates=\(wcSettings.eventpassdates)&search=\(searchText.stringByAddingPercentEscapesForQueryValue()!)"
        makeHTTPPostRequest(Path.POSTDATA, callback: callback, url: url, body: body)
    }
    
    //forcepickup
    func forcePickup(authToken:String, ticket:String, wcSettings: wcsettings, callback: APICallback){
        var uuid = NSUUID().UUIDString
        let deviceid = "1";
        let url = "https://www.flavorus.com/API.aspx"
        let body = "authtoken=\(authToken.stringByAddingPercentEscapesForQueryValue()!)&deviceid=\(deviceid)&devicetype=ios&salt=\(uuid)&call=ForcePickup&events=\(wcSettings.events)&eventpassdates=\(wcSettings.eventpassdates)&Ticket=\(ticket.stringByAddingPercentEscapesForQueryValue()!)"
        makeHTTPPostRequest(Path.POSTDATA, callback: callback, url: url, body: body)
    }
    
    //encodeCard
    func encodeCard(authToken:String, encodetoken:String, cardnumber: String, callback: APICallback){
        var uuid = NSUUID().UUIDString
        let deviceid = "1";
        let url = "https://www.flavorus.com/API.aspx"
        let body = "authtoken=\(authToken.stringByAddingPercentEscapesForQueryValue()!)&deviceid=\(deviceid)&devicetype=ios&salt=\(uuid)&call=EncodeCard&encodetoken=\(encodetoken)&cardnumber=\(cardnumber)"
        makeHTTPPostRequest(Path.POSTDATA, callback: callback, url: url, body: body)
    }
    
    
    //scansettings
    func scanTicket(authToken:String, ticketnum: String, scanSettings: scansettings, callback: APICallback){
        var uuid = NSUUID().UUIDString
        let deviceid = "1";
        let url = "https://www.flavorus.com/API.aspx"
        let body = "authtoken=\(authToken.stringByAddingPercentEscapesForQueryValue()!)&deviceid=\(deviceid)&devicetype=ios&salt=\(uuid)&call=ScanTicket&ticketnum=\(ticketnum.stringByAddingPercentEscapesForQueryValue()!)&events=\(scanSettings.events)&tickettypes=\(scanSettings.tickettypes)&eventpassdates=\(scanSettings.eventpassdates)&attractions=\(scanSettings.attractions)&mode=\(scanSettings.mode)"
        println(body)
        makeHTTPPostRequest(Path.POSTDATA, callback: callback, url: url, body: body)
    }
    
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse) {
        let httpResponse = response as! NSHTTPURLResponse
        statusCode = httpResponse.statusCode
        switch (httpResponse.statusCode) {
        case 201, 200, 401:
            self.responseData.length = 0
        default:
            println("ignore")
        }
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        self.responseData.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection) {
        var error: NSError?
        var json : AnyObject! = NSJSONSerialization.JSONObjectWithData(self.responseData, options: NSJSONReadingOptions.MutableLeaves, error: &error)
        if (error != nil) {
            callback(nil, error)
            return
        }
        
        switch(statusCode, self.path!) {
        case (200, Path.POSTDATA):
            callback(self.handleJSON(json), nil)
        case (401, _):
            callback(nil, handleAuthError(json))
        default:
            // Unknown Error
            callback(nil, nil)
        }
    }
    
    func handleAuthError(json: AnyObject) -> NSError {
        if let resultObj = json as? JSONDictionary {
            // beta2 workaround
            if let messageObj: AnyObject = resultObj["error"] {
                if let message = messageObj as? String {
                    return NSError(domain:"signIn", code:401, userInfo:["error": message])
                }
            }
        }
        return NSError(domain:"signIn", code:401, userInfo:["error": "unknown auth error"])
    }
    
    func handleJSON(json: AnyObject) -> JSON {
        return JSON(json)
    }
    
    // private
    func makeHTTPGetRequest(path: Path, callback: APICallback, url: NSString) {
        self.path = path
        self.callback = callback
        let request = NSURLRequest(URL: NSURL(string: url as String)!)
        let conn = NSURLConnection(request: request, delegate:self)
        if (conn == nil) {
            callback(nil, nil)
        }
    }
    
    func makeHTTPPostRequest(path: Path, callback: APICallback, url: NSString, body: NSString) {
        self.path = path
        self.callback = callback
        let request = NSMutableURLRequest(URL: NSURL(string: url as String)!)
        request.HTTPMethod = "POST"
        request.HTTPBody = body.dataUsingEncoding(NSUTF8StringEncoding)
        let conn = NSURLConnection(request: request, delegate:self)
        if (conn == nil) {
            callback(nil, nil)
        }
    }
    
    /*func sha256(data : NSData) -> NSData {
        var hash = [UInt8](count: Int(CC_SHA256_DIGEST_LENGTH), repeatedValue: 0)
        CC_SHA256(data.bytes, CC_LONG(data.length), &hash)
        let res = NSData(bytes: hash, length: Int(CC_SHA256_DIGEST_LENGTH))
        return res
    }*/
}

extension String {
    
    /// Percent escape value to be added to a URL query value as specified in RFC 3986
    ///
    /// This percent-escapes all characters except the alphanumeric character set and "-", ".", "_", and "~".
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// :returns: Return precent escaped string.
    
    func stringByAddingPercentEscapesForQueryValue() -> String? {
        let characterSet = NSMutableCharacterSet.alphanumericCharacterSet()
        characterSet.addCharactersInString("-._~")
        return stringByAddingPercentEncodingWithAllowedCharacters(characterSet)
    }
}

