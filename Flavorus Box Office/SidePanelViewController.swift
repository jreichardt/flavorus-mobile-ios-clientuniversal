//
//  LeftViewController.swift
//  SlideOutNavigation
//
//  Created by James Frost on 03/08/2014.
//  Copyright (c) 2014 James Frost. All rights reserved.
//

import UIKit

@objc
protocol SidePanelViewControllerDelegate {
  func signoutSelected()
}

class SidePanelViewController: UIViewController {
  
  var delegate: SidePanelViewControllerDelegate?

  
  override func viewDidLoad() {
    super.viewDidLoad()
    
  }
    
    @IBAction func SignOutButton(sender: AnyObject) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainController = storyboard.instantiateViewControllerWithIdentifier("Login") as! UIViewController
        appDelegate.window?.rootViewController = mainController
        
    }
  
}
