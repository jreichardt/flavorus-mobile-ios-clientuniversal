//
//  LoginViewController.swift
//  Flavorus Box Office
//
//  Created by admin on 4/28/15.
//  Copyright (c) 2015 Flavorus. All rights reserved.
//

import UIKit
import CryptoSwift
import CoreData

class LoginViewController: UIViewController {
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        /*var user:NSManagedObject = NSManagedObject()
        
        //--- try and pull from core data
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        //2
        let fetchRequest = NSFetchRequest(entityName:"User")
        
        //3
        var error: NSError?
        
        let fetchedResults =
        managedContext.executeFetchRequest(fetchRequest,
            error: &error) as? [NSManagedObject]
        
        for result in fetchedResults! as [NSManagedObject] {
            if result.valueForKey("authtoken") as? String != "" {
                //skip login page
                
            }
        }
        
        if let results = fetchedResults {
            
            
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }*/

        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        var user = appDelegate.getCoreDataUser()
        if user != nil {
            let authtoken = user!.valueForKey("authtoken") as? String
            println(authtoken)
            
            NSUserDefaults.standardUserDefaults().setObject(authtoken, forKey:"AuthToken")
            NSUserDefaults.standardUserDefaults().synchronize()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainController = storyboard.instantiateViewControllerWithIdentifier("EventTableView") as! UIViewController
            appDelegate.window?.rootViewController = mainController
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBAction func signInButtonPushed(sender: AnyObject) {
        //let passwordstr = "iGoBgj7dqMLfAyV+8nKlj+hySImx55WDonfa5PZ/XjM="
        
        var email:String = txtEmail.text as String
        var password:String = txtPassword.text as String

        //var hash2 = password.sha256()
        //let utf16str = password.dataUsingEncoding(NSUTF16StringEncoding)
        //var utf8stra = password.dataUsingEncoding(NSUTF8StringEncoding)
        //let hashedpassword = CryptoSwift.Hash.sha256(utf16str!).calculate()
        //        let hashedpassword2 = CryptoSwift.Hash.sha256(utf8stra!).calculate()
        //let base = hashedpassword!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        //if let base64Encoded = hashedpassword?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        //{
            if (true){
                var api = API()
                api.signIn(email, password: password, callback: { (data, error) in
                    if (error == nil) {
                        // do stuff with data
                        if (data["success"] == false)
                        {
                            let messagestr = data["message"].stringValue
                            
                            
                            var alert = UIAlertController(title: "Error", message: messagestr, preferredStyle: UIAlertControllerStyle.Alert)
                            alert.addAction(UIAlertAction(title: "close", style: UIAlertActionStyle.Default, handler: nil))
                            self.presentViewController(alert, animated: true, completion: nil)
                            
                            
                        }else{
                            let name = data["Name"].stringValue
                            let authToken = data["AuthToken"].stringValue
                            let device = data["Device"].stringValue
                            
                            NSUserDefaults.standardUserDefaults().setObject(authToken, forKey:"AuthToken")
                            NSUserDefaults.standardUserDefaults().synchronize()
                            
                            
                            
                            //1
                            let appDelegate =
                            UIApplication.sharedApplication().delegate as! AppDelegate
                            

                            //--- blow out old data
                            appDelegate.clearCoreDataUser()
                            appDelegate.clearCoreDataEvents()
                            
                            let managedContext = appDelegate.managedObjectContext!
                            
                            //2
                            let entity =  NSEntityDescription.entityForName("User",
                                inManagedObjectContext:
                                managedContext)
                            
                            let user = NSManagedObject(entity: entity!,
                                insertIntoManagedObjectContext:managedContext)
                            
                            //3
                            user.setValue(email, forKey: "email")
                            user.setValue(authToken, forKey: "authtoken")
                            user.setValue(password, forKey: "password")
                            user.setValue(name, forKey: "name")
                            user.setValue(device, forKey: "terminalid")
                            
                            
                            //4
                            var error: NSError?
                            if !managedContext.save(&error) {
                                println("Could not save \(error), \(error?.userInfo)")
                            }

                            
                            //let entryViewController:MainTabViewController = MainTabViewController()
                            //self.presentViewController(entryViewController, animated: true, completion: nil)
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let mainController = storyboard.instantiateViewControllerWithIdentifier("EventTableView") as! UIViewController
                            appDelegate.window?.rootViewController = mainController
                            
                            
                        }
                    }
                })
                
            }
        //}
        
        
    }
    
    
    
}
