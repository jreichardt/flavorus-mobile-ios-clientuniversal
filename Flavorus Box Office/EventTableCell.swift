//
//  EventTableCell.swift
//  Flavorus Box Office
//
//  Created by Lydia Reichardt on 5/28/15.
//  Copyright (c) 2015 Flavorus. All rights reserved.
//


import UIKit
import SwiftyJSON


class EventTableCell: UITableViewCell {
    
   
    @IBOutlet weak var DateMonthText: UILabel!
    @IBOutlet weak var DateDayText: UILabel!
    @IBOutlet weak var EventNameText: UILabel!
    @IBOutlet weak var VenueText: UILabel!
    @IBOutlet weak var FullDateText: UILabel!
    
    @IBOutlet weak var PassImg1: UIView!
    @IBOutlet weak var PassImg2: UIView!
    @IBOutlet weak var PassImg3: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

