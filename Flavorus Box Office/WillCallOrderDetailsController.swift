//
//  WillCallOrderDetailsController.swift
//  Flavorus Box Office
//
//  Created by admin on 6/8/15.
//  Copyright (c) 2015 Flavorus. All rights reserved.
//

import UIKit
import SwiftyJSON

class WillCallOrderDetailsController: UITableViewController, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate  {

    
    var items: [JSON] = []
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        /*self.items = []
        for (key, eventjson) in self.items["Orders"] {
            self.items.append(eventjson)
        }
        */
        //self.tableView.reloadData()
        
                
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.items.count
        
    }
    
    override
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:WillCallOrderDetailsCell = self.tableView.dequeueReusableCellWithIdentifier("WillCallOrderDetailsCell") as! WillCallOrderDetailsCell
        
        
        var jsonOrder:JSON
        
        
        
        jsonOrder = self.items[indexPath.row]
        
        cell.TicketType.text = jsonOrder["TicketTypeName"].stringValue

        var ticketCount:Int = 0
        var ticketCountReceived:Int = 0
        for (key,cjson) in jsonOrder["Tickets"] {
            ticketCount += 1
            if cjson["IsPickedUp"].stringValue == "true" {
                   ticketCountReceived += 1
            }
        }

        
        if ticketCount > 0 && ticketCount == ticketCountReceived
        {
            
            cell.Quantity.textColor = UIColor.whiteColor()
            cell.QuantBoxView.backgroundColor = UIColor(netHex:0x009CC7)
            cell.Quantity.text = String(ticketCount)
        }else{
            cell.Quantity.text = String(ticketCount)
        }
        
       
        
        return cell
    }
    
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var jsonEvent:JSON
        
        jsonEvent = self.items[indexPath.row]
        
        
    }
    
}
