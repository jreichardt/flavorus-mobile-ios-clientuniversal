//
//  EventViewController.swift
//  Flavorus Box Office
//
//  Created by Lydia Reichardt on 5/29/15.
//  Copyright (c) 2015 Flavorus. All rights reserved.
//

import UIKit

@objc
protocol EventViewControllerDelegate {
    optional func toggleLeftPanel()
    optional func collapseSidePanel()
}

class EventViewController: UIViewController {
    
    var delegate: EventViewControllerDelegate?

    @IBOutlet weak var EventContainerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "some title"
        
        
    }

    
    // MARK: Button actions
    @IBAction func settingsTapped(sender: AnyObject) {
        delegate?.toggleLeftPanel?()
    }

    
}

extension EventViewController: SidePanelViewControllerDelegate {
    
    func signoutSelected() {
        delegate?.collapseSidePanel?()
    }
}