//
//  WillCallController.swift
//  Flavorus Box Office
//
//  Created by admin on 6/5/15.
//  Copyright (c) 2015 Flavorus. All rights reserved.
//

import UIKit
import SwiftyJSON

class ServiceController: UITableViewController, UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate  {
    
    var items: [JSON] = []
    var events: [JSON] = []
    var authToken: String = ""
    var wcsettings:API.wcsettings = API.wcsettings()
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        searchBar.delegate = self
        
        // Do any additional setup after loading the view, typically from a nib.
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        
        let key = "AuthToken"
        authToken = (NSUserDefaults.standardUserDefaults().objectForKey(key)) as! String
        
        if authToken == "" //Check for first run of app
        {
            //bad!!
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainController = storyboard.instantiateViewControllerWithIdentifier("Login") as! UIViewController
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.window?.rootViewController = mainController
            return
        }
        
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        if appDelegate.events != nil {
            self.events = appDelegate.events!
            wcsettings.events = getEvents()
            
            self.navigationController!.navigationBar.barTintColor = UIColor(netHex:0xD3202D)
            //self.title = self.events[0]["Name"].stringValue
        }
        
        
        
        
    }
    
    func refreshTableFromAPI(sender:AnyObject, searchText: String)
    {
        var api = API()
        api.willCallOrders(authToken, searchText: searchText, wcSettings: wcsettings, callback: { (data, error) in
            if (error == nil) {
                // do stuff with data
                if (data["success"] == false)
                {
                    let messagestr = data["message"].stringValue
                    
                    var alert = UIAlertController(title: "Error", message: messagestr, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "close", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                    
                }else{
                    self.items = []
                    for (key, eventjson) in data["Orders"]["Customers"] {
                        self.items.append(eventjson)
                    }
                    
                    self.tableView.reloadData()
                }
            }
        })
        
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        
    }
    
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
    
    func searchBarSearchButtonClicked( searchBar: UISearchBar)
    {
        let searchText = searchBar.text
        if count(searchText) < 2
        {
            self.items = []
            self.tableView.reloadData()
        }else{
            refreshTableFromAPI(self, searchText: searchBar.text)
            
            self.tableView.reloadData()
        }
        self.view.endEditing(true)
    }
    
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.items.count
        
    }
    
    override
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell:WillcallCell = self.tableView.dequeueReusableCellWithIdentifier("WillcallCell") as! WillcallCell
        
        
        var jsonOrder:JSON
        
        
        
        jsonOrder = self.items[indexPath.row]
        
        cell.Last4.text = jsonOrder["CCLastFour"].stringValue
        cell.NameText.text = jsonOrder["LastName"].stringValue + ", " + jsonOrder["FirstName"].stringValue
        cell.EventName.text = getEventName(jsonOrder["Event"].stringValue) //--- get the event name from the events json, is there a better way?
        let ticketCount:Int = getTicketCount(jsonOrder)
        let ticketCountReceived:Int = getTicketCountReceived(jsonOrder)
        if ticketCount > 0 && ticketCount == ticketCountReceived
        {
            
            cell.Quantity.textColor = UIColor.whiteColor()
            cell.QuantBoxView.backgroundColor = UIColor(netHex:0x009CC7)
            cell.Quantity.text = String(ticketCount)
        }else{
            cell.Quantity.text = String(ticketCount)
        }
        
        
        return cell
    }
    
    func getTicketCountReceived(jsonOrder: JSON) -> Int
    {
        var quant:Int = 0
        for (key,cjson) in jsonOrder["TicketTypes"] {
            for (key,tjson) in cjson["Tickets"] {
                if tjson["IsPickedUp"].stringValue == "true" {
                    quant += 1
                }
            }
        }
        return quant
    }
    
    func getTicketCount(jsonOrder: JSON) -> Int
    {
        var quant:Int = 0
        for (key,cjson) in jsonOrder["TicketTypes"] {
            quant += cjson["Tickets"].count
        }
        return quant
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var jsonOrder:JSON
        
        jsonOrder = self.items[indexPath.row]
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let orderDetails = storyboard.instantiateViewControllerWithIdentifier("WillCallOrderDetailsContainerView") as! WillCallOrderDetailsContainerView
        for (key,cjson) in jsonOrder["TicketTypes"] {
            orderDetails.items.append(cjson)
        }
        orderDetails.parentController = self
        orderDetails.nameString = jsonOrder["LastName"].stringValue + ", " + jsonOrder["FirstName"].stringValue
        orderDetails.last4String = jsonOrder["CCLastFour"].stringValue
        self.navigationController!.pushViewController(orderDetails, animated: true)
    }
    
    func getEvents() -> String {
        var eventstring = ""
        for (eventjson) in self.events {
            if (eventstring != "") { eventstring = eventstring + "," }
            eventstring += eventjson["ID"].stringValue
        }
        return eventstring
    }
    
    func getEventName(eventID: String) -> String {
        for (eventjson) in self.events {
            if eventjson["ID"].stringValue == eventID
            {
                return eventjson["Name"].stringValue
            }
        }
        return ""
    }
    
}
