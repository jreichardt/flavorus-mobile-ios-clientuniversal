//
//  WillCallOrderDetailsContainerView.swift
//  Flavorus Box Office
//
//  Created by admin on 6/9/15.
//  Copyright (c) 2015 Flavorus. All rights reserved.
//


import UIKit
import SwiftyJSON

class WillCallOrderDetailsContainerView: UIViewController {
    
    @IBOutlet weak var TableViewContainer: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var last4: UILabel!
    
    var nameString: String = ""
    var last4String: String = ""
    var items: [JSON] = []
    var parentController: WillCallController = WillCallController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        name.text = nameString
        last4.text = last4String

        // create the table view controller
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let orderDetailsTable = storyboard.instantiateViewControllerWithIdentifier("WillCallOrderDetailsTable") as! WillCallOrderDetailsController
        orderDetailsTable.items = self.items
        self.TableViewContainer.addSubview(orderDetailsTable.view)
    }
    
    
    @IBAction func CheckInButtonPushed(sender: AnyObject) {
        //--- check in this order for willcall
        var api = API()
        
        var ticket: String = self.items[0]["Tickets"][0]["TicketID"].stringValue
        var authToken:String = parentController.authToken
        var wcSettings: API.wcsettings = parentController.wcsettings
        
        api.forcePickup(authToken, ticket: ticket, wcSettings: wcSettings, callback: { (data, error) in
            if (error == nil) {
                // do stuff with data
                if (data["success"] == false)
                {
                    let messagestr = data["message"].stringValue
                    
                    var alert = UIAlertController(title: "Error", message: messagestr, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "close", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                    
                }else{
                    //---- check to see if there are magcards/RFID/Ticketstock to encode
                    
                    /*
                    {
                    "success": true,
                    "dispensed": 4,
                    "encodetoken": "24d6d4d1-ebbc-4702-aefe-351e41652bb2",
                    "encodingtickets": [
                    "60225373",
                    "60225332",
                    "60225333",
                    "60225329"
                    ],
                    "displayvalues": [
                    {
                    "t": 60225329,
                    "tt": "Regular",
                    "mc": "https://cdn.vor.us/images/flavorus-swipe-card.png"
                    },
                    {
                    "t": 60225332,
                    "tt": "Regular",
                    "mc": "https://cdn.vor.us/images/flavorus-swipe-card.png"
                    },
                    {
                    "t": 60225333,
                    "tt": "Regular",
                    "mc": "https://cdn.vor.us/images/flavorus-swipe-card.png"
                    },
                    {
                    "t": 60225373,
                    "tt": "Regular",
                    "mc": "https://cdn.vor.us/images/flavorus-swipe-card.png"
                    }
                    ]
                    }
*/
                    if data["dispensed"] > 0 {
                        //--- success we might need to encode
                        if data["encodetoken"].stringValue != "" {
                            var encodeToken:String = data["encodetoken"].stringValue
                            //-- popup encode path
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let encodeView = storyboard.instantiateViewControllerWithIdentifier("EncodeViewController") as! EncodeViewController
                            encodeView.authToken = authToken
                            encodeView.encodeToken = encodeToken
                            encodeView.encodingtickets = data["displayvalues"]
                            self.presentViewController(encodeView, animated: true, completion: nil)
                            //self.navigationController!.pushViewController(encodeView, animated: true)
                            
                        }
                    }
                    
                }
            }
        })

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

