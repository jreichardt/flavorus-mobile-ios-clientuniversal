//
//  EncodeViewController.swift
//  Flavorus Box Office
//
//  Created by James Reichardt on 7/6/15.
//  Copyright (c) 2015 Flavorus. All rights reserved.
//



import UIKit
import SwiftyJSON

class EncodeViewController: UIViewController {
    
    var encodingtickets: JSON = nil
    var encodeToken: String = ""
    var authToken: String = ""
    var currentIndex: Int = 0
    var scanner : DTDevices = DTDevices()

    @IBOutlet weak var TicketType: UILabel!
    @IBOutlet weak var MagcardText: UILabel!
    @IBOutlet weak var MagcardImage: UIImageView!
    
    
    deinit {
        
    }
    
    override func viewDidAppear(animated: Bool) {
        // init linea class and connect it
        scanner.addDelegate(self)
        scanner.connect()
    }
    
    override func viewDidDisappear(animated: Bool) {
        scanner.disconnect()
    }
    
    override func viewDidLoad() {
        
        
        
        showNextMagcard()
        
        super.viewDidLoad()
    }
    
  
    
    @IBAction func SkipButtonClicked(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func showNextMagcard()
    {
        if encodingtickets.count > 0 && currentIndex < encodingtickets.count {
            MagcardText.text = "Magcard \(currentIndex+1) of \(encodingtickets.count)"
            TicketType.text = encodingtickets[currentIndex]["tt"].stringValue
            MagcardImage.contentMode = UIViewContentMode.ScaleAspectFit
            if let checkedUrl = NSURL(string: encodingtickets[currentIndex]["mc"].stringValue) {
                downloadImage(checkedUrl)
            }
        }else{
            // we are done. move along
            dismissViewControllerAnimated(true, completion: nil)
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func barcodeData (barcode: String, type: Int) {
        encodeTicket(barcode)
    }

    func encodeTicket(cardnum: String) {
        var api = API()
        api.encodeCard(authToken, encodetoken: self.encodeToken, cardnumber: cardnum, callback: { (data, error) in
            if (error == nil) {
                // do stuff with data
                if (data["success"] == false)
                {
                    let messagestr = data["message"].stringValue
                    
                    var alert = UIAlertController(title: "Error", message: messagestr, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "close", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                    
                    
                }else{
                    // do success stuff... move on to the next magcard
                    self.currentIndex += 1
                    self.showNextMagcard()
                }
            }
        })
    }
    
    
    //--- async image code
    func getDataFromUrl(urL:NSURL, completion: ((data: NSData?) -> Void)) {
        NSURLSession.sharedSession().dataTaskWithURL(urL) { (data, response, error) in
            completion(data: data)
            }.resume()
    }
    func downloadImage(url:NSURL){
        println("Started downloading \"\(url.lastPathComponent!.stringByDeletingPathExtension)\".")
        getDataFromUrl(url) { data in
            dispatch_async(dispatch_get_main_queue()) {
                println("Finished downloading \"\(url.lastPathComponent!.stringByDeletingPathExtension)\".")
                self.MagcardImage.image = UIImage(data: data!)
            }
        }
    }
    
}

