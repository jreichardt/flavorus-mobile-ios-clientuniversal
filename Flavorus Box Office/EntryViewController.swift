//
//  EntryViewController.swift
//  Ticketon Box Office
//
//  Created by admin on 4/28/15.
//  Copyright (c) 2015 Ticketon. All rights reserved.
//

import UIKit
import SwiftyJSON

class EntryViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var ManualTextField: UITextField!
    @IBOutlet weak var ScanResultsView: UIView!
    
    var authToken: String = ""
    var events: [JSON] = []
    var scansettings:API.scansettings = API.scansettings()
    var scanner : DTDevices = DTDevices()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view, typically from a nib.
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let key = "AuthToken"
        authToken = (NSUserDefaults.standardUserDefaults().objectForKey(key)) as! String
        
        if authToken == "" //Check for first run of app
        {
            //bad!!
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainController = storyboard.instantiateViewControllerWithIdentifier("Login") as! UIViewController
            appDelegate.window?.rootViewController = mainController
            return
        }
        
        if appDelegate.events != nil {
            self.events = appDelegate.events!
            scansettings.events = getEvents()
        }
        
        self.ManualTextField.delegate = self;
        
    }
    
    override func viewDidAppear(animated: Bool) {
        // init linea class and connect it
        scanner.addDelegate(self)
        scanner.connect()
    }
    
    override func viewDidDisappear(animated: Bool) {
        scanner.disconnect()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        let ticketnum = textField.text
        if (ticketnum != "")
        {
            println(ticketnum)
            ScanTicket(ticketnum)
        }
        return false
    }
    
    func barcodeData (barcode: String, type: Int) {
        
        // You can use this data as you wish
        // Here I write barcode data into the console
        //println("Barcode Data: \(barcode)")
        ScanTicket(barcode)
    }
    
    func magneticCardRawData(tracks: NSData) {
        println(tracks)
        
        //self.ManualTextField.text = track1
        //ScanTicket(barcode)
    }
    
    @IBAction func ScanBarcodeButton(sender: AnyObject) {
        // this is the camera view
        let barcodeViewController:BarcodeImageScannerViewController = BarcodeImageScannerViewController()
        barcodeViewController.scansettings = scansettings
        barcodeViewController.authToken = authToken
        
        self.presentViewController(barcodeViewController, animated: true, completion: nil)
        
    }
    

    func ScanTicket(ticketnum: String){
        var api = API()
        
        api.scanTicket(authToken, ticketnum: ticketnum, scanSettings: scansettings, callback: { (data, error) in
            if (error == nil) {
                
                if let viewWithTag = self.ScanResultsView.viewWithTag(100) {
                    viewWithTag.removeFromSuperview()
                }
                
                // do stuff with data
                if (data["success"] == false)
                {
                    // bad scan
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let scanResultsController = storyboard.instantiateViewControllerWithIdentifier("EntryScanResultsTable") as! EntryScanResultsViewController
                    scanResultsController.tableView.backgroundColor = UIColor .clearColor()
                    scanResultsController.items.append(data)
                    let scanview = scanResultsController.view
                    self.ScanResultsView.addSubview(scanview)
                    
                }else{
                    
                    //show subview
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let scanResultsController = storyboard.instantiateViewControllerWithIdentifier("EntryScanResultsTable") as! EntryScanResultsViewController
                    scanResultsController.tableView.backgroundColor = UIColor .clearColor()
                    for (key, ticketjson) in data["tickets"] {
                        scanResultsController.items.append(ticketjson)
                    }
                    let scanview = scanResultsController.view
                    self.ScanResultsView.addSubview(scanview)
                    
                }
            }
        })
        
    }
    
    func getEvents() -> String {
        var eventstring = ""
        for (eventjson) in self.events {
            if (eventstring != "") { eventstring = eventstring + "," }
            eventstring += eventjson["ID"].stringValue
        }
        return eventstring
    }
    
}
