import UIKit
import AVFoundation

protocol StartBarcodeDelegate {
    func StartRunningBarcodeSession(controller: EntryScanResultsViewController)
}


class BarcodeImageScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate, StartBarcodeDelegate {
    
    let session         : AVCaptureSession = AVCaptureSession()
    var previewLayer    : AVCaptureVideoPreviewLayer!
    var ScanResultsView   : UIView = UIView()
    var scansettings    : API.scansettings = API.scansettings()
    var authToken       : String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        // For the sake of discussion this is the camera
        let device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        
        // Create a nilable NSError to hand off to the next method.
        // Make sure to use the "var" keyword and not "let"
        var error : NSError? = nil
        
        
        let input : AVCaptureDeviceInput? = AVCaptureDeviceInput.deviceInputWithDevice(device, error: &error) as? AVCaptureDeviceInput
        
        // If our input is not nil then add it to the session, otherwise we're kind of done!
        if input != nil {
            session.addInput(input)
        }
        else {
            
            println(error)
            dismissViewControllerAnimated(true, completion: nil)
            return
        }
        
        let output = AVCaptureMetadataOutput()
        output.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
        session.addOutput(output)
        output.metadataObjectTypes = output.availableMetadataObjectTypes
        
        
        previewLayer = AVCaptureVideoPreviewLayer.layerWithSession(session) as! AVCaptureVideoPreviewLayer
        previewLayer.frame = self.view.bounds
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.view.layer.addSublayer(previewLayer)
        
        // Start the scanner. You'll have to end it yourself later.
        session.startRunning()
        
        
        // Allow the view to resize freely
        self.ScanResultsView.autoresizingMask =   UIViewAutoresizing.FlexibleTopMargin |
            UIViewAutoresizing.FlexibleBottomMargin |
            UIViewAutoresizing.FlexibleLeftMargin |
            UIViewAutoresizing.FlexibleRightMargin
        
        
        
        // Add it to our controller's view as a subview.
        self.view.addSubview(self.ScanResultsView)
        
    }
    
    // This is called when we find a known barcode type with the camera.
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        
        var highlightViewRect = CGRectZero
        
        var barCodeObject : AVMetadataObject!
        
        var detectionString : String!
        
        let barCodeTypes = [AVMetadataObjectTypeUPCECode,
            AVMetadataObjectTypeCode39Code,
            AVMetadataObjectTypeCode39Mod43Code,
            AVMetadataObjectTypeEAN13Code,
            AVMetadataObjectTypeEAN8Code,
            AVMetadataObjectTypeCode93Code,
            AVMetadataObjectTypeCode128Code,
            AVMetadataObjectTypePDF417Code,
            AVMetadataObjectTypeQRCode,
            AVMetadataObjectTypeAztecCode
        ]
        
        
        // The scanner is capable of capturing multiple 2-dimensional barcodes in one scan.
        for metadata in metadataObjects {
            
            for barcodeType in barCodeTypes {
                
                if metadata.type == barcodeType {
                    barCodeObject = self.previewLayer.transformedMetadataObjectForMetadataObject(metadata as! AVMetadataMachineReadableCodeObject)
                    
                    //highlightViewRect = barCodeObject.bounds
                    
                    detectionString = (metadata as! AVMetadataMachineReadableCodeObject).stringValue
                    self.session.stopRunning()
                    ScanTicket(detectionString)
                    break
                }
                
            }
        }
        
        //println(detectionString)
        //self.highlightView.frame = highlightViewRect
        //self.view.bringSubviewToFront(self.highlightView)
        
    }
    
    func ScanTicket(ticketnum: String){
        var api = API()
        api.scanTicket(authToken, ticketnum: ticketnum, scanSettings: scansettings, callback: { (data, error) in
            if (error == nil) {
                
                if let viewWithTag = self.ScanResultsView.viewWithTag(100) {
                    viewWithTag.removeFromSuperview()
                }
                
                // do stuff with data
                if (data["success"] == false)
                {
                    // bad scan
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let scanResultsController = storyboard.instantiateViewControllerWithIdentifier("EntryScanResultsTable") as! EntryScanResultsViewController
                    scanResultsController.tableView.backgroundColor = UIColor .clearColor()
                    scanResultsController.items.append(data)
                    let scanview = scanResultsController.view
                    scanResultsController.startBarcodeDeligate = self
                    self.ScanResultsView.addSubview(scanview)
                    
                    //self.session.startRunning()

                    
                }else{
                    
                    //show subview
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let scanResultsController = storyboard.instantiateViewControllerWithIdentifier("EntryScanResultsTable") as! EntryScanResultsViewController
                    scanResultsController.tableView.backgroundColor = UIColor .clearColor()
                    for (key, ticketjson) in data["tickets"] {
                        scanResultsController.items.append(ticketjson)
                    }
                    scanResultsController.startBarcodeDeligate = self
                    let scanview = scanResultsController.view
                    self.ScanResultsView.addSubview(scanview)
                    
                    //self.session.startRunning()

                }
            } else {
              //--- API error....
                //--- todo: handle this with a warning or something.
                self.session.startRunning()
            }
            
        })
        
    }
    
    func StartRunningBarcodeSession(controller: EntryScanResultsViewController){
        self.session.startRunning()
    }
    
    
    
}